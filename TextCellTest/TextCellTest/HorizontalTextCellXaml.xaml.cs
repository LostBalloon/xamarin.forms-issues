﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TextCellTest
{
	public partial class HorizontalTextCellXaml : ContentView
	{
		public HorizontalTextCellXaml ()
		{
			InitializeComponent ();
		}
	}

	public class HorizontalTextCell : ViewCell
	{
		public HorizontalTextCell () {
			View = new HorizontalTextCellXaml ();
			View.BindingContext = this;
			this.IsEnabled = false;
		}

		public static readonly BindableProperty TitleProperty = 
			BindableProperty.Create<HorizontalTextCell,string> (
				p => p.Title, default(string));

		public string Title {
			get { return (string)GetValue (TitleProperty); }
			set { SetValue (TitleProperty, value); }
		}

		public static readonly BindableProperty DetailProperty = 
			BindableProperty.Create<HorizontalTextCell,string> (
				p => p.Detail, default(string));

		public string Detail {
			get { return (string)GetValue (DetailProperty); }
			set { SetValue (DetailProperty, value); }
		}
	}
}

