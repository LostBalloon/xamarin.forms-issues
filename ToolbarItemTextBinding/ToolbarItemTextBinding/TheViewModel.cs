﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace ToolbarItemTextBinding
{
	public class TheViewModel : INotifyPropertyChanged
	{
		private string _title;
		private string _toolbarText;

		public TheViewModel ()
		{
			_title = "A";
			_toolbarText = "a";
		}

		public String Title {
			get {
				return _title;
			}
			set {
				if (value != _title) {
					_title = value;
					OnPropertyChanged ();
				}
			}
		}

		public String ToolbarText {
			get {
				return _toolbarText;
			}
			set {
				if (value != _toolbarText) {
					_toolbarText = value;
					OnPropertyChanged ();
				}
			}
		}

		public ICommand ChangeTextCommand {
			get { return new Command(() => { Title += "A"; ToolbarText += "a"; }); }
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler (this, new PropertyChangedEventArgs (propertyName));
		}

		#endregion
	}
}

