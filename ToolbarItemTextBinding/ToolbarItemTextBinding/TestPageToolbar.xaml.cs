﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ToolbarItemTextBinding
{
	public partial class TestPageToolbar : ContentPage
	{
		public TestPageToolbar ()
		{
			InitializeComponent ();
			BindingContext = new TheViewModel ();
		}
	}
}

